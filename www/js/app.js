angular.module('partner',
 ['ionic',
 'restangular',
 'ionic-material'
]).run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
}).config([
  '$stateProvider',
  '$urlRouterProvider',
  'RestangularProvider',
  function($stateProvider, $urlRouterProvider,RestangularProvider) {

    $urlRouterProvider.otherwise('/tab/dash');

  }
]).factory('APIRestangular',function(Restangular){
  var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  /*
  handler.setDefaultHeaders({
    'api': config.api,
    'token': $auth.getToken()
  });*/
  handler.setBaseUrl('http://192.168.1.34:3000/api/');
  return handler;
});


