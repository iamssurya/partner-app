'use strict';
angular.module('partner').config([
  '$stateProvider',
  function($stateProvider) {
    $stateProvider.state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    }).state('tab.dash', {
      url: '/dash',
      templateUrl: 'templates/tab-dash.html'
    }).state('tab.goals', {
      url: '/goals',
      templateUrl: 'templates/tab-goals.html',
      controller: 'GoalController'
    }).state('tab.goals.new', {
      url: '/new',
      templateUrl: 'templates/tab-goals-new.html',
      controllerAs: 'GoalController'
    }).state('tab.goals.list', {
      url: '/list/:filter',
      templateUrl: 'templates/tab-goals-list.html',
      controllerAs: 'GoalController'
    }).state('tab.account', {
      url: '/account',
      templateUrl: 'templates/tab-account.html'
    });
  }
]);
angular.module('partner').controller('MainController',[
  '$scope',
  '$http',
  '$state',
  '$ionicPopup',
  '$timeout',
  'ionicMaterialInk', 
  'ionicMaterialMotion', 
  '$ionicSideMenuDelegate',
  function($scope,$http,$state,$ionicPopup,$timeout,ionicMaterialInk, ionicMaterialMotion, $ionicSideMenuDelegate){
    console.log('MainController');
    $scope.name = 'surya';
    $scope.age = '12';
    $scope.toggleLeft = function() {
      console.log('tolle');
      $ionicSideMenuDelegate.toggleLeft();
    };
  }
]); 

angular.module('partner').controller('GoalController',[
  '$scope',
  '$http',
  '$state',
  '$ionicPopup',
  '$timeout',
  '$ionicSideMenuDelegate',
  'APIRestangular',
  function($scope,$http,$state,$ionicPopup,$timeout,$ionicSideMenuDelegate,APIRestangular){
    console.log('GoalController');
    $scope.partner = {};
    $scope.partner.list = 'all';
    $scope.partner.goals = {};
    
    if($state.current.name == 'tab.goals.list'){
      console.log($state.params);
      $scope.partner.list = $state.params.filter;
    }
    if($state.current.name == 'tab.goals.new'){
      console.log("new");
    }
    APIRestangular.all('goals').getList().then(function(response){
      $scope.partner.goals = response;
      console.log($scope.partner.goals);
    });
    $http({
      method: 'GET',
      url: 'http://192.168.1.34:3000/api/goals'
    },function(response){
      console.log(response);
    });
  }
]); 
